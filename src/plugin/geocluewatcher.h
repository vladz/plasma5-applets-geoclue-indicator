/*
 * Copyright 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>

class GeoClueWatcher : public QObject
{
    Q_OBJECT
    Q_PROPERTY(State state READ state NOTIFY stateChanged)

public:
    enum State { Active, Inactive, Unavailable };
    Q_ENUM(State)

    explicit GeoClueWatcher(QObject *parent = nullptr);
    ~GeoClueWatcher() override;

    State state() const;
    void setState(State state);

Q_SIGNALS:
    void stateChanged();

private Q_SLOTS:
    void handlePropertiesChanged(const QString &interfaceName,
                                 const QVariantMap &changedProperties,
                                 const QStringList &invalidatedProperties);
    void handleServiceRegistered();
    void handleServiceUnregistered();

private:
    void updateProperties(const QVariantMap &properties);

    State m_state = Unavailable;
};
