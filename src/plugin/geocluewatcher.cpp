/*
 * Copyright 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "geocluewatcher.h"

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusServiceWatcher>
#include <QDBusPendingCallWatcher>
#include <QDBusPendingReply>

static const QString s_serviceName = QStringLiteral("org.freedesktop.GeoClue2");
static const QString s_geoClueManagerPath = QStringLiteral("/org/freedesktop/GeoClue2/Manager");
static const QString s_geoClueManagerInterface = QStringLiteral("org.freedesktop.GeoClue2.Manager");
static const QString s_propertiesInterface = QStringLiteral("org.freedesktop.DBus.Properties");

GeoClueWatcher::GeoClueWatcher(QObject *parent)
    : QObject(parent)
{
    auto watcher = new QDBusServiceWatcher(s_serviceName, QDBusConnection::systemBus(),
                                           QDBusServiceWatcher::WatchForOwnerChange, this);

    connect(watcher, &QDBusServiceWatcher::serviceRegistered,
            this, &GeoClueWatcher::handleServiceRegistered);
    connect(watcher, &QDBusServiceWatcher::serviceUnregistered,
            this, &GeoClueWatcher::handleServiceUnregistered);

    handleServiceRegistered();
}

GeoClueWatcher::~GeoClueWatcher()
{
}

GeoClueWatcher::State GeoClueWatcher::state() const
{
    return m_state;
}

void GeoClueWatcher::setState(State state)
{
    if (m_state == state)
        return;
    m_state = state;
    emit stateChanged();
}

void GeoClueWatcher::handleServiceRegistered()
{
    QDBusConnection bus = QDBusConnection::systemBus();

    const bool connected = bus.connect(s_serviceName, s_geoClueManagerPath, s_propertiesInterface,
                                       QStringLiteral("PropertiesChanged"),
                                       this, SLOT(handlePropertiesChanged(QString,QVariantMap,QStringList)));
    if (!connected)
        return;

    QDBusMessage message = QDBusMessage::createMethodCall(s_serviceName,
                                                          s_geoClueManagerPath,
                                                          s_propertiesInterface,
                                                          QStringLiteral("GetAll"));
    message.setArguments({ s_geoClueManagerInterface });

    QDBusPendingReply<QVariantMap> properties = bus.asyncCall(message);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(properties, this);

    connect(watcher, &QDBusPendingCallWatcher::finished, this, [this](QDBusPendingCallWatcher *self) {
        self->deleteLater();

        const QDBusPendingReply<QVariantMap> properties = *self;
        if (properties.isError())
            return;

        updateProperties(properties.value());
    });
}

void GeoClueWatcher::handleServiceUnregistered()
{
    setState(Unavailable);
}

void GeoClueWatcher::handlePropertiesChanged(const QString &interfaceName,
                                             const QVariantMap &changedProperties,
                                             const QStringList &invalidatedProperties)
{
    Q_UNUSED(invalidatedProperties)

    if (interfaceName != s_geoClueManagerInterface)
        return;
    if (!changedProperties.contains(QStringLiteral("InUse")))
        return;

    updateProperties(changedProperties);
}

void GeoClueWatcher::updateProperties(const QVariantMap &properties)
{
    const QVariant activeProperty = properties.value(QStringLiteral("InUse"));
    if (activeProperty.isValid())
        setState(activeProperty.toBool() ? Active : Inactive);
}
