/*
 * Copyright 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5

import org.kde.kquickcontrolsaddons 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0

import org.kde.plasma.private.geoclueindicator 1.0

Item {
    id: root

    Plasmoid.icon: "mark-location"
    Plasmoid.status: {
        switch (watcher.state) {
        case GeoClueWatcher.Active:
            return PlasmaCore.Types.ActiveStatus;
        case GeoClueWatcher.Inactive:
            return PlasmaCore.Types.HiddenStatus;
        case GeoClueWatcher.Unavailable:
            return PlasmaCore.Types.PassiveStatus;
        }
    }

    Plasmoid.toolTipMainText: i18n("GeoClue")
    Plasmoid.toolTipSubText: {
        switch (watcher.state) {
        case GeoClueWatcher.Active:
            return i18n("Active");
        case GeoClueWatcher.Inactive:
            return i18n("Inactive");
        case GeoClueWatcher.Unavailable:
            return i18n("Unavailable");
        }
    }

    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation
    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        source: plasmoid.icon
    }

    GeoClueWatcher { id: watcher }
}
